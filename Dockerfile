FROM node:10.15-alpine

RUN apk update && apk upgrade && apk add --no-cache bash git openssh gcc g++ make
RUN apk add --no-cache openssl-dev
RUN apk add --no-cache python
RUN apk add --no-cache libc6-compat

COPY ./src ./src
COPY ./test ./test
COPY ./package.json ./package.json

RUN npm install mocha -g
RUN npm install

RUN mocha ./test/test.js

CMD [ "node", "./src/start.js"]


# Decoder
Decodes any incoming message based on the javascript decoding function defined for the device sending that message.

For every message received, it gets the associated device decode javascript function from a redis DB.

If message is decoded properly, it is sent on kafka. If not, an alert is sent on kafka.

## Tests
Tests in `test/test.js`.
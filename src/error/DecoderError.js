#!/usr/bin/env nodejs

'use strict';

class DecoderError {
  constructor(msg, code, deivceId) {
    console.error(msg);
    this.code = code;
    this.deviceId = deivceId;
    this.timestamp = (new Date()).getTime();
  }
}


module.exports.DecoderError = DecoderError;

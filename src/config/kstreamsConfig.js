#!/usr/bin/env nodejs

'use strict';

const config = {
  noptions: {
    "metadata.broker.list": process.env.KAFKA_BROKER_LIST || "127.0.0.1:9092",
    "group.id": process.env.KAFKA_GROUP_ID || "decoderService",
    "client.id": process.env.KAFKA_CLIENT_ID || "decoderService",
    "event_cb": true,
    "compression.codec": "snappy",
    "api.version.request": true,
    "socket.keepalive.enable": true,
    "socket.blocking.max.ms": 100,
    "enable.auto.commit": false,
    "auto.commit.interval.ms": 100,
    "heartbeat.interval.ms": 250,
    "retry.backoff.ms": 250,
    "fetch.min.bytes": 100,
    "fetch.message.max.bytes": 2 * 1024 * 1024,
    "queued.min.messages": 100,
    "fetch.error.backoff.ms": 100,
    "queued.max.messages.kbytes": 50,
    "fetch.wait.max.ms": 1000,
    "queue.buffering.max.ms": 1000,
    "batch.num.messages": 10000
  },
  tconf: {
    "auto.offset.reset": "latest",
    "request.required.acks": 1
  },
  batchOptions: {
    "batchSize": 1,
    "commitEveryNBatch": 1,
    "concurrency": 1,
    "commitSync": false,
    "noBatchCommits": false
  }
};

module.exports = config;

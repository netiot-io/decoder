#!/usr/bin/env nodejs

'use strict';

const config = {
  port: process.env.REDIS_PORT || 6379,
  address: process.env.REDIS_ADDRESS || "127.0.0.1"
};

module.exports = config;

#!/usr/bin/env nodejs

'use strict';

const config = {
  port: process.env.HTTP_PORT || 80
};

module.exports = config;

#!/usr/bin/env nodejs

'use strict';

const kstreamConfig = require('./config/kstreamsConfig');
const { KafkaStreams } = require('kafka-streams');
const { DecoderError } = require('./error/DecoderError.js');

class TransformKstreams {
  constructor() {
    this.kafkaStreams = new KafkaStreams(kstreamConfig);
    this.rawDataTopic = process.env.KAFKA_RAW_DATA_TOPIC || "rawData";
    this.rawDataStream = this.kafkaStreams.getKStream(this.rawDataTopic);
    this.deviceDataTopic = process.env.KAFKA_DEVICE_DATA_TOPIC || "deviceData";
    this.deviceDataStream = this.kafkaStreams.getKStream(this.deviceDataTopic);
    this.outputDataTopic = process.env.KAFKA_OUTPUT_DATA_TOPIC || "outputData";
    this.outputDataStream = this.kafkaStreams.getKStream(null);
    this.outputDataStream.to(this.outputDataTopic);
    this.errorDataTopic = process.env.KAFKA_ERROR_DATA_TOPIC || "errorData";
    this.errorDataStream = this.kafkaStreams.getKStream(null);
    this.errorDataStream.to(this.errorDataTopic);
    this.rawDataTransform = null;
    this.deviceDataTransform = null;
  }

  Start() {
    this.kafkaStreams.on("error", error => {
      console.log("Error occured on Kafka:", error.message);
    });

    this.rawDataStream
      .from(this.rawDataTopic)
      .forEach(message => TransformKstreams.TransformMessage(message, this.rawDataTransform));

    this.deviceDataStream
      .from(this.deviceDataTopic)
      .forEach(message => TransformKstreams.TransformMessage(message, this.deviceDataTransform));

    this.rawDataStream.start();
    this.deviceDataStream.start();
    this.outputDataStream.start();
    this.errorDataStream.start();
  }

  static TransformMessage(message, callback) {
    callback(JSON.parse(message.value.toString('utf8')));
  }

  Stop() {
    this.kafkaStreams.closeAll().then(() => console.log("Closed Kafka Streams!"));
  }

  SendData(key, value) {
    let message = {
      key: key,
      value: Buffer.from(JSON.stringify(value)),
      timestamp: (new Date()).getTime()
    };

    console.debug(`Message: ${JSON.stringify(message)}`);

    if (!(value instanceof DecoderError))
      this.outputDataStream.writeToStream(message);
    else
      this.errorDataStream.writeToStream(message);
  }
}

module.exports.TransformKstreams = TransformKstreams;

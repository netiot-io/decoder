#!/usr/bin/env nodejs

'use strict';

const syntaxError = 'interpreter.badsyntax';
const executionError = 'interpreter.executionfailed';
const timeoutError = 'interpreter.executiontimeout';
const formatError = 'interpreter.wrongFormat';

const Redis = require('redis');
const Express = require('express');
const BodyParser = require('body-parser');
const {VM} = require('vm2');

const httpConfig = require('./config/httpConfig.js');
const redisConfig = require('./config/redisConfig.js');
const { TransformKstreams } = require('./TransformKstreams.js');
const { DecoderError } = require('./error/DecoderError');


function InitializeRedis() {
  let redisClient = Redis.createClient(redisConfig.port, redisConfig.address);

  redisClient.on('error', function (err) {
    console.error(`Redis error event - ${redisClient.host}:${redisClient.port} - ${err}`);
  });

  return redisClient;
}

function verifyDecodeInfo(decodeInfo) {
  if (decodeInfo === null) {
    console.error('Decode information was empty!');

    return null;
  }

  decodeInfo = JSON.parse(decodeInfo);

  if (decodeInfo === undefined || decodeInfo.decoderCode === undefined) {
    console.error('Decoder code is not a valid string!');

    return null;
  }

  return decodeInfo;
}

let service = null;

class DecoderService {
  constructor() {
    this.redisClient = null;
    this.transformKstreams = null;
    this.endPoint = Express();
  }

  static GetInstance() {
    if (service === null)
      service = new DecoderService();

    return service;
  }

  InitializeTestEndpoint() {
    let service = this;

    this.server = this.endPoint.listen(httpConfig.port, () => {
      console.log(`Listening at http://${service.server.address().address}:${service.server.address().port}`);
    });

    this.endPoint.post('/decoder', BodyParser.urlencoded({ extended: false }), function (req, res) {
      req.body.data = Array.from(req.body.data);
      service.ParsePayload(req.body);
      res.send('Ok!');
    });

    this.endPoint.get('/health', function (req, res) {
      res.sendStatus(200);
    });
  }

  Initialize() {
    this.InitializeTestEndpoint();
    this.redisClient = InitializeRedis();
    this.transformKstreams = new TransformKstreams();
    this.transformKstreams.rawDataTransform = DecoderService.TransformRawData;
    this.transformKstreams.deviceDataTransform = DecoderService.TransformDeviceData;
    this.transformKstreams.Start();

    let service = this;

    process.on('SIGINT', () => service.CleanUp());
  }

  CleanUp() {
    let service = this;

    service.transformKstreams.Stop();
    service.server.close();
    service.redisClient.quit(function (err, res) {
      console.log('Closed Redis connection!');
    });

  }

  static TransformRawData(data) {
    if (Array.isArray(data.devices))
      data.devices.forEach(x => DecoderService.GetInstance().ParsePayload(x));
  }

  static TransformDeviceData(data) {
    data.data = data.data_source;
    delete data.data_source;
    DecoderService.GetInstance().ParsePayload(data);
  }

  static ExecuteCode(code, sensorData) {
    let decodeFunction;
    let parsedData;

    let vm = new VM({
      timeout: 1000,
      sandbox: {}
    });

    try {
      decodeFunction = vm.run(code);
    } catch (err) {
      if (err.message.includes("timed out"))
        return new DecoderError('Timeout occurred while compiling the function!', timeoutError, sensorData.deviceId);
      else
        return new DecoderError('An error occurred while compiling the function!', syntaxError, sensorData.deviceId);
    }

    let data = sensorData.data;

    vm = new VM({
      timeout: 1000,
      sandbox: {decodeFunction, data}
    });

    try {
      parsedData = vm.run('decodeFunction(data)');
    } catch (err) {
      if (err.message.includes("timed out"))
        return new DecoderError('Timeout occurred while executing the function!', timeoutError, sensorData.deviceId);
      else
        return new DecoderError('An error occurred while executing the function!', executionError, sensorData.deviceId);
    }

    return parsedData;
  }

  DecodeData(decoderStr, sensorData) {
    let parsedData = DecoderService.ExecuteCode(decoderStr, sensorData);

    if (parsedData instanceof DecoderError) {
      this.SendResult(parsedData);

      return;
    }

    try {
      if (typeof(parsedData) === 'string')
        parsedData = JSON.parse(parsedData);
    } catch (err) {
      parsedData = new DecoderError('Resulted value has the wrong format!', formatError, sensorData.deviceId);

      this.SendResult(parsedData);

      return;
    }

    parsedData.deviceId = sensorData.deviceId;
    parsedData.timestamp_r = sensorData.timestamp;

    if (sensorData.deviceName !== undefined)
      parsedData.deviceName = sensorData.deviceName;

    if (!('timestamp' in parsedData))
      parsedData.timestamp = parsedData.timestamp_r;

    console.log(`${new Date().toISOString()} Parsed data: ${JSON.stringify(parsedData)}`);

    this.SendResult(parsedData);
  }

  ParsePayload(sensorData) {
    let service = this;

    this.redisClient.hget('devices', sensorData.deviceId, function (err, reply) {
      if (err !== null) {
        console.error('Redis returned an error: ');
        console.error(err);

        return;
      }

      reply = verifyDecodeInfo(reply);

      if (reply === null)
        return;

      service.DecodeData(reply.decoderCode, sensorData);
    });
  }

  SendResult(parsedData) {
    this.transformKstreams.SendData(parsedData.deviceId, parsedData);
  }
}

module.exports.DecoderService = DecoderService;
